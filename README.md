# Microstructure reconstruction using TensorFlow-based artificial intelligence

Meeting minutes and intermediate tasks can be found in [related Google Document](https://docs.google.com/document/d/1lVnbIV5eK0jiA9YkxpgCwTvzsFBMfmPSGO3WqzfQxkI/edit?usp=sharing).

Resources:
* [Google Machine Learning Crash Course](https://developers.google.com/machine-learning/crash-course/)
* [1Blue3Brown explanatory videos](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)
* [AI by Google](https://ai.google/education)
* [Udacity](https://eu.udacity.com/courses/all) and these courses in particular: 
   * [AI programming with Python](https://eu.udacity.com/course/ai-programming-python-nanodegree--nd089)
   * [Deep Learning by Google](https://eu.udacity.com/course/deep-learning--ud730)
   * [Deep Learning](https://eu.udacity.com/course/deep-learning-nanodegree--nd101)
* [scikit -- alternative to TensorFlow](http://scikit-learn.org/stable/)
* [TensorFlow tutorials](https://www.tensorflow.org/versions/r1.3/get_started/input_fn)
   * [Save and load trained networks](https://cv-tricks.com/tensorflow-tutorial/save-restore-tensorflow-models-quick-complete-tutorial/)  

Other explanatory pages:
* [Neural Networks & Deep Learning](http://neuralnetworksanddeeplearning.com/)
* [Neural Networks, Manifolds, and Topology](http://colah.github.io/posts/2014-03-NN-Manifolds-Topology/) (and [Colah's blog](https://colah.github.io/) in general)
* [Backpropagation](https://google-developers.appspot.com/machine-learning/crash-course/backprop-scroll/)
* [Keras with Tensorflow Course - DeepLizard](https://www.youtube.com/watch?v=qFJeN9V1ZsI)
* [Difference between Accuracy and Loss](https://www.baeldung.com/cs/ml-loss-accuracy)

Authors:
* Kryštof Latka (@latka-krystof)
* Martin Folwarczny (@martin.folw)
* Martin Doškář (@MartinDoskar)
* Jan Zeman (@jan.zeman4)

