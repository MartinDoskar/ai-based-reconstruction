#!/usr/bin/env python3

import numpy as np
import math
import random

# make a 2D array from a vector of values when pixels are in RGB/RGBA format
# Correction: Now specifically suited to the newly generated images. In order to 
# change it back to the RGBA format, replace 191 with 255.
def make_grid_RGBA(arr):
    pixels = list()
    for i in range(len(arr)):
        pixels.append(int(arr[i][0] / 191))
    pix = np.array(pixels, dtype=int)
    pix = np.reshape(pix, (int(math.sqrt(len(arr))), int(math.sqrt(len(arr)))))
    return pix

# generate random pixels, leaving just the edges of the original pixture unchanged
def make_random_grid(arr, windowSize):
    pixels = list()
    for i in range(len(arr)):
        value = int(arr[i][0] / 191)
        pixels.append(value)
    pix = np.array(pixels, dtype=int)
    pix = np.reshape(pix, (int(math.sqrt(len(arr))), int(math.sqrt(len(arr)))))

    for i in range(windowSize // 2, len(pix)):
        for j in range(windowSize // 2, len(pix[0]) - windowSize // 2):
            pix[i][j] = random.randint(0, 1)
    
    return pix

# extract non-causal neighbourhood with introduced error
def extract_2D_full_neighborhood_with_error(i, j, arr, windowSize, error):
    inputs = np.empty((windowSize, windowSize))
    for x in range(i - windowSize // 2, i +  windowSize // 2 + 1):
        for y in range(j - windowSize // 2, j + windowSize // 2 + 1):
            if x != i and y != j:
                inputs[x - (i - windowSize // 2)][y - (j - windowSize // 2)] = arr[x][y]
            else:
                inputs[x - (i - windowSize // 2)][y - (j - windowSize // 2)] = random.randint(0, 1)
    
    for x in range(len(inputs)):
        for y in range(len(inputs[0])):
            if random.uniform(0, 1) < error:
                inputs[x][y] = random.randint(0, 1)

    return inputs

# extract causal neighbourhood without introduced error
def extract_2D_neighborhood(i, j, arr, windowSize):
    inputs = np.empty((windowSize // 2 + 1, windowSize))
    for x in range(i - windowSize // 2, i + 1):
        for y in range(j - windowSize // 2, j + windowSize // 2 + 1):
            if x != i or y < j:
                inputs[x - (i - windowSize // 2)][y - (j - windowSize // 2)] = arr[x][y]

    for y in range(0, windowSize // 2 + 1):
        inputs[windowSize // 2][windowSize // 2 + y] = random.randint(0, 1)

    return inputs

# extract causal neighbourhood with introduced error
def extract_2D_neighborhood_with_error(i, j, arr, windowSize, error):
    inputs = np.empty((windowSize // 2 + 1, windowSize))
    for x in range(i - windowSize // 2, i + 1):
        for y in range(j - windowSize // 2, j + windowSize // 2 + 1):
            if x != i or y < j:
                inputs[x - (i - windowSize // 2)][y - (j - windowSize // 2)] = arr[x][y]

    for y in range(0, windowSize // 2 + 1):
        inputs[windowSize // 2][windowSize // 2 + y] = random.randint(0, 1)

    for x in range(len(inputs)):
        for y in range(len(inputs[0])):
            if random.uniform(0, 1) < error:
                inputs[x][y] = random.randint(0, 1)

    return inputs

# extract training data for the causal model without introduced error
def extract_2D_data(arr, windowSize):
    inputs = list()
    outputs = list()

    for i in range(windowSize // 2, len(arr)):
        for j in range(windowSize // 2, len(arr[0]) - windowSize // 2): 
            inputArr = extract_2D_neighborhood(i, j, arr, windowSize)
            inputs.append(inputArr)
            outputs.append(arr[i, j])

    return np.asarray(inputs), np.asarray(outputs)

# extract training data for the causal model with introduced error
def extract_2D_data_with_error(arr, windowSize, error):
    inputs = list()
    outputs = list()

    for i in range(windowSize // 2, len(arr)):
        for j in range(windowSize // 2, len(arr[0]) - windowSize // 2): 
            inputArr = extract_2D_neighborhood_with_error(i, j, arr, windowSize, error)
            inputs.append(inputArr)
            outputs.append(arr[i, j])

    return np.asarray(inputs), np.asarray(outputs)

# extract training data for the non-causal model
def extract_2D_full_data_with_error(arr, windowSize, error):
    inputs = list()
    outputs = list()

    for i in range(windowSize // 2, len(arr) - windowSize // 2):
        for j in range(windowSize // 2, len(arr[0]) - windowSize // 2): 
            inputArr = extract_2D_full_neighborhood_with_error(i, j, arr, windowSize, error)
            inputs.append(inputArr)
            outputs.append(arr[i, j])

    return np.asarray(inputs), np.asarray(outputs)

# make predictions using the causal neighbourhood
def predict_from_random_2D(model, arr, windowSize):
    probabilities = list()
    input = list()
    for i in range(windowSize // 2, len(arr)):
        for j in range(windowSize // 2, len(arr[0]) - windowSize // 2):
            neighbourhood = extract_2D_neighborhood(i, j, arr, windowSize)
            input.append(neighbourhood)
            inp = np.asarray(input)
            shape = np.shape(inp)
            inp = np.reshape(inp, (1, shape[1], shape[2], 1))
            prediction = model(inp)
            rounded_prediction = np.argmax(prediction, axis=-1)
            probabilities.append(prediction[0][1])
            arr[i][j] = rounded_prediction[0]
            input.clear()
    
    reconstruct_image = np.empty((len(arr), len(arr[0]), 3), dtype=np.uint8)
    for i in range(len(reconstruct_image)):
        for j in range(len(reconstruct_image[0])):
            reconstruct_image[i][j][0] = arr[i][j] * 255
            reconstruct_image[i][j][1] = arr[i][j] * 255
            reconstruct_image[i][j][2] = arr[i][j] * 255

    return reconstruct_image, probabilities

# make predictions using the full non-causal neighbourhood
def predict_from_random_2D_full(model, arr, windowSize):
    probabilities = list()
    input = list()
    for i in range(windowSize // 2, len(arr) - windowSize // 2):
        for j in range(windowSize // 2, len(arr[0]) - windowSize // 2):
            neighbourhood = extract_2D_full_neighborhood_with_error(i, j, arr, windowSize, 0)
            input.append(neighbourhood) 
            inp = np.asarray(input)
            shape = np.shape(inp)
            inp = np.reshape(inp, (1, shape[1], shape[2], 1))
            prediction = model(inp)
            rounded_prediction = np.argmax(prediction, axis=-1)
            probabilities.append(prediction[0][1])
            arr[i][j] = rounded_prediction[0]
            input.clear()
    
    reconstruct_image = np.empty((len(arr), len(arr[0]), 3), dtype=np.uint8)
    for i in range(len(reconstruct_image)):
        for j in range(len(reconstruct_image[0])):
            reconstruct_image[i][j][0] = arr[i][j] * 255
            reconstruct_image[i][j][1] = arr[i][j] * 255
            reconstruct_image[i][j][2] = arr[i][j] * 255

    return reconstruct_image, probabilities


