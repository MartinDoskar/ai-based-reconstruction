#!/usr/bin/env python3

import numpy as np
from PIL import Image
from image_manipulation import make_grid_RGBA
from numpy import linalg

# two-point correlation function
def S2(inData):
    """Compute two-point probability function."""
    tranformedData = np.fft.fft2(inData)
    productData = np.multiply(tranformedData, np.conj(tranformedData))
    outData = np.divide(np.absolute(np.fft.ifft2(productData)), inData.size)

    return outData

# volume representation function
def volume_representation(inData):
    sum = np.sum(inData)
    
    return sum / np.size(inData)

orig_image = './dat/foam_v3_blackandwhite.png'
orig_im = Image.open(orig_image)
orig_arr = list(orig_im.getdata())
orig_picture = make_grid_RGBA(orig_arr)

reconst_image = './dat/checkerboards/test_checkerboard.png'
reconst_im = Image.open(reconst_image)
reconst_arr = list(reconst_im.getdata())
reconst_picture = make_grid_RGBA(reconst_arr)

# compute the epsilon-S2 error
def RMSE(original, reconstructed):
    difference = S2(original) - S2(reconstructed)
    norm = linalg.norm(difference)

    return norm / (len(original) * len(original[0]))

def extract_sum_of_neighbours(arr, i, j):
    sum = 0
    for x in range(i - 1, i + 2):
        for y in range(j - 1, j + 2):
            sum += np.abs(arr[x][y] - arr[i][j])
    
    return sum / 8

# compute the epsilon-D error
def smoothing_classification(inData):
    sum = 0
    for i in range(1, len(inData) - 1):
        for j in range(1, len(inData[0]) - 1):
            sum += extract_sum_of_neighbours(inData, i, j)

    return sum / ((len(inData) - 2) * (len(inData[0]) - 2))




