#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from PIL import Image
import os.path

from image_manipulation import extract_2D_data, make_grid_RGBA, make_random_grid, predict_from_random_2D
from data_analysis import rounded_predictions

# window size must be an odd number
windowSize = 21
every_nth_element = 5
poolingSize = 3

# Dataset for reconstruction
test_image = './dat/foam_v3_200x200.png'
test_im = Image.open(test_image)
test_arr = list(test_im.getdata())
test_picture = make_random_grid(test_arr, windowSize)

# background = './dat/sample02.png'
# background_im = Image.open(background)
# background_arr = list(background_im.getdata())
# test_picture = make_grid_preprocessed(background_arr)

# test_inputs, test_outputs = extract_2D_data(test_picture, windowSize)
# number_of_inputs = np.shape(test_inputs)
# test_inputs = np.reshape(test_inputs, (number_of_inputs[0], number_of_inputs[1], number_of_inputs[2], 1))

# Just for testing, to see the input the image to the model
shape = len(test_picture)
im_input = np.empty((shape, shape, 3), dtype=np.uint8)
for i in range(len(im_input)):
    for j in range(len(im_input[0])):
        im_input[i][j][0] = test_picture[i][j] * 255
        im_input[i][j][1] = test_picture[i][j] * 255
        im_input[i][j][2] = test_picture[i][j] * 255

# Load model instead of training it each time 
model = tf.keras.models.load_model('./testing_models/2_layers_16_neurons_21_3x3.h5')

# # Use the model to predict
# predictions = model.predict(x = test_inputs, batch_size=100)
# probabilities = np.delete(predictions, 0, 1)

# rounded_predictions = np.argmax(predictions, axis=-1)

# # Reconstruct an image from the predictions
# shape = int(math.sqrt(len(rounded_predictions)))
# reconstruct_image = np.empty((shape, shape, 3), dtype=np.uint8)
# for i in range(len(reconstruct_image)):
#     for j in range(len(reconstruct_image[0])):
#         reconstruct_image[i][j][0] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255
#         reconstruct_image[i][j][1] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255
#         reconstruct_image[i][j][2] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255

# Predict from random 
reconstruct_image, probabilities = predict_from_random_2D(model, test_picture, windowSize)

# Save the input image
im_test = Image.fromarray(im_input)
if os.path.isfile('./dat/foam_v3_blackandwhite.png') is False:
    im_test.save('./dat/foam_v3_blackandwhite.png')

# Save the image
im = Image.fromarray(reconstruct_image)
if os.path.isfile('./dat/reconstructions/testing/2_layers_16_neurons_21_3x3.png') is False:
    im.save('./dat/reconstructions/testing/2_layers_16_neurons_21_3x3.png')

# Plot the probability histogram
probabilities = np.array(probabilities)
plt.hist(probabilities, bins=100, range=(0.0, 1.0))
plt.xlabel('Probability')
plt.ylabel('Frequency')
plt.title('Sponge Reconstruction - Probability Histogram')
plt.show()
