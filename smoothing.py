#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from PIL import Image
import os.path

from image_manipulation import extract_2D_data, make_grid_RGBA, make_random_grid, predict_from_random_2D, predict_from_random_2D_full
from data_analysis import rounded_predictions

# window size must be an odd number
windowSize = 15

# Dataset for reconstruction
test_image = './dat/reconstructions/testing/4_layers_16_neurons_31_3x3.png'
test_im = Image.open(test_image)
test_arr = list(test_im.getdata())
test_picture = make_grid_RGBA(test_arr)

# background = './dat/sample02.png'
# background_im = Image.open(background)
# background_arr = list(background_im.getdata())
# test_picture = make_grid_preprocessed(background_arr)

# test_inputs, test_outputs = extract_2D_data(test_picture, windowSize)
# number_of_inputs = np.shape(test_inputs)
# test_inputs = np.reshape(test_inputs, (number_of_inputs[0], number_of_inputs[1], number_of_inputs[2], 1))

# Load model instead of training it each time 
model = tf.keras.models.load_model('./smoothing_models/15_error_0.05_average.h5')

# # Use the model to predict
# predictions = model.predict(x = test_inputs, batch_size=100)
# probabilities = np.delete(predictions, 0, 1)

# rounded_predictions = np.argmax(predictions, axis=-1)

# # Reconstruct an image from the predictions
# shape = int(math.sqrt(len(rounded_predictions)))
# reconstruct_image = np.empty((shape, shape, 3), dtype=np.uint8)
# for i in range(len(reconstruct_image)):
#     for j in range(len(reconstruct_image[0])):
#         reconstruct_image[i][j][0] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255
#         reconstruct_image[i][j][1] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255
#         reconstruct_image[i][j][2] = rounded_predictions[len(reconstruct_image[0]) * i + j] * 255

# Predict from random 
reconstruct_image, probabilities = predict_from_random_2D_full(model, test_picture, windowSize)

# Save the image
im = Image.fromarray(reconstruct_image)
if os.path.isfile('./dat/test.png') is False:
    im.save('./dat/test.png')

# Plot the probability histogram
probabilities = np.array(probabilities)
plt.hist(probabilities, bins=100, range=(0.0, 1.0))
plt.xlabel('Probability')
plt.ylabel('Frequency')
plt.title('Sponge Reconstruction - Probability Histogram')
plt.show()
