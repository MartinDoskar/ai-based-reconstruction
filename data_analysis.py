#!/usr/bin/env python3

# check if there are any duplicate inputs (identical neighbourhoods) to the neural network
def check_duplicate_inputs(inputs, predictions):
    unique = dict()
    for i in range(len(inputs)):
        neighbourhood = tuple(inputs[i])
        if neighbourhood not in unique:
            unique[neighbourhood] = predictions[i]
        elif unique[neighbourhood] != predictions[i]:
            print('There is a different output for two same inputs at index ' + str(i))
    return unique

def rounded_predictions(probabilities):
    for i in range(len(probabilities)):
        if probabilities[i] > 0.50:
            probabilities[i] = 1
        else:
            probabilities[i] = 0
    return probabilities

