#!/usr/bin/env python3

import numpy as np
import tensorflow as tf
from PIL import Image
import os.path

from image_manipulation import extract_2D_data, extract_2D_data_with_error, make_grid_RGBA, make_random_grid
from data_analysis import rounded_predictions

# window size must be an odd number
windowSize = 21
every_nth_element = 5
error = 0.00

# Extract training data from image
train_image = './dat/foam_v1_500x500.png'
train_im = Image.open(train_image)
train_arr = list(train_im.getdata())
train_picture = make_grid_RGBA(train_arr)

# Extract validation data from another image
validation_image = './dat/foam_v2_500x500.png'
validation_im = Image.open(validation_image)
validation_arr = list(validation_im.getdata())
validation_picture = make_grid_RGBA(validation_arr)

inputs, outputs = extract_2D_data(train_picture, windowSize)
validation_inputs, validation_outputs = extract_2D_data(validation_picture, windowSize)

number_of_inputs = np.shape(inputs)
inputs = np.reshape(inputs, (number_of_inputs[0], number_of_inputs[1], number_of_inputs[2], 1))
validation_inputs = np.reshape(validation_inputs, (number_of_inputs[0], number_of_inputs[1], number_of_inputs[2], 1))

model = tf.keras.Sequential([
    # tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=((number_of_inputs[1], number_of_inputs[2], 1))),
    tf.keras.layers.MaxPool2D((3, 3)),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(2, activation='softmax')
])

model.compile(optimizer='Adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x=inputs, y=outputs, epochs=30, verbose=2, batch_size=100)

validation_loss, validation_acc = model.evaluate(validation_inputs, validation_outputs, verbose=1)
print('\nTest accuracy:', validation_acc)

# Save the trained model
if os.path.isfile('./testing_models/2_layers_16_neurons_21_3x3.h5') is False:
    model.save('./testing_models/2_layers_16_neurons_21_3x3.h5')
