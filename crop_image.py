#!/usr/bin/env python3

from PIL import Image
import numpy as np
import math
import os

side = 500

def crop_image(arr, side):
    pixels = list()
    for i in range(len(arr)):
        pixels.append(arr[i][0])
    pix = np.array(pixels, dtype=int)
    pix = np.reshape(pix, (int(math.sqrt(len(arr))), int(math.sqrt(len(arr)))))
    
    crop = np.empty((side, side))
    for i in range(side):
        for j in range(side):
            crop[i][j] = pix[i][j]
    return pix

image = './dat/foam_W16-4-4_10x10_v2.png'
im = Image.open(image)
arr = list(im.getdata())
crop = crop_image(arr, side)

im_crop = np.empty((side, side, 3), dtype=np.uint8)
for i in range(len(im_crop)):
    for j in range(len(im_crop[0])):
        im_crop[i][j][0] = crop[i][j]
        im_crop[i][j][1] = crop[i][j]
        im_crop[i][j][2] = crop[i][j]

save_crop = Image.fromarray(im_crop)
if os.path.isfile('./dat/foam_v2_500x500.png') is False:
    save_crop.save('./dat/foam_v2_500x500.png')